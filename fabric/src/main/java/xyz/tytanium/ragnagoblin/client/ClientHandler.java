package xyz.tytanium.ragnagoblin.client;

import com.mrcrayfish.goblintraders.client.renderer.entity.GoblinTraderRenderer;
import com.mrcrayfish.goblintraders.client.renderer.entity.model.GoblinTraderModel;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.itemgroup.v1.ItemGroupEvents;
import net.minecraft.world.item.CreativeModeTabs;
import xyz.tytanium.ragnagoblin.client.renderer.entity.RagnaGoblinModelLayer;
import xyz.tytanium.ragnagoblin.core.ModEntities;
import xyz.tytanium.ragnagoblin.core.ModItems;

public class ClientHandler implements ClientModInitializer
{
    @Override
    public void onInitializeClient()
    {
        EntityRendererRegistry.register(ModEntities.RAGNA_GOBLIN.get(), GoblinTraderRenderer::new);
        EntityModelLayerRegistry.registerModelLayer(RagnaGoblinModelLayer.RAGNA_GOBLIN, GoblinTraderModel::createBodyLayer);
        ItemGroupEvents.modifyEntriesEvent(CreativeModeTabs.SPAWN_EGGS).register(entries -> {
            entries.accept(ModItems.RAGNA_GOBLIN_SPAWN_EGG.get());
        });
    }
}

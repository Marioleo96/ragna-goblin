package xyz.tytanium.ragnagoblin;

import com.mrcrayfish.goblintraders.entity.AbstractGoblinEntity;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import xyz.tytanium.ragnagoblin.core.ModEntities;

@Mod(Constants.MOD_ID)
public class RagnaGoblin
{

    public RagnaGoblin()
    {
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        bus.addListener(this::onCommonSetup);
        bus.addListener(this::onEntityAttributeCreation);
    }

    private void onCommonSetup(FMLCommonSetupEvent event)
    {
        event.enqueueWork(Bootstrap::init);
    }

    private void onEntityAttributeCreation(EntityAttributeCreationEvent event)
    {
        event.put(ModEntities.RAGNA_GOBLIN.get(), AbstractGoblinEntity.createAttributes().build());
    }
}
